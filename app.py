import logging

from flask import Flask

from src.route.handler_atm import handler_atm

logging.basicConfig(
    filename="mobiquity-coding-exercise.log", level=logging.ERROR
)


app = Flask(__name__)

if __name__ == "__main__":
    app.register_blueprint(handler_atm)
    
    app.run(host='0.0.0.0', port=8080)
