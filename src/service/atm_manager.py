import json
import uuid
import requests
import cerberus

from src.helper.custom_exceptions import ValidationException

with open("schema/atm.json") as fp:
    atm_schema = json.load(fp)


class AtmManager:
    """Service class to interact with the mock in-memory db.
    This class is also responsible to laod the initial ING ATMs dataset
    """

    def __init__(self) -> None:
        self.atm_store = []
        self.load_atm_store()

    def load_atm_store(self):
        """Laod the initial ING ATMs dataset"""
        r = requests.get("https://www.ing.nl/api/locator/atms/", timeout=30)

        resp = r.text[5:]

        atms = json.loads(resp)

        for atm in atms:
            atm.update({"id": uuid.uuid4().hex})

        self.atm_store = atms

    def save(self, data):
        """Validate and save an atm record

        :data: atm payload.
        """
        validator = cerberus.Validator(allow_unknown=True)
        validator.validate(data, atm_schema)

        if bool(validator.errors):
            raise ValidationException(str(validator.errors))
        else:
            data["id"] = uuid.uuid4().hex

            self.atm_store.append(data)

            return data["id"]

    def update(self, id, data):
        """Validate and update an atm record

        :id: atm identifier.

        :data: atm payload.
        """
        validator = cerberus.Validator(allow_unknown=True)
        validator.validate(data, atm_schema)

        if bool(validator.errors):
            raise ValidationException(str(validator.errors))
        elif not any(atm["id"] == id for atm in self.atm_store):
            raise ValidationException(f"No record exists with id {id}")
        else:
            for idx, atm in enumerate(self.atm_store):
                if atm["id"] == id:
                    data["id"] = id
                    self.atm_store[idx] = data

        return data

    def delete_by_id(self, id):
        """Delete an atm record by id

        :id: atm identifier.
        """
        self.atm_store = list(
            filter(
                lambda atm: atm["id"] != id,
                self.atm_store))

    def get_atms(self, city=None):
        """find atm records

        :city: optional city name to be filtered by.
        """
        atm_store = None

        if city:
            atm_store = [
                atm
                for atm in self.atm_store
                if "address" in atm
                and "city" in atm["address"]
                and atm["address"]["city"].lower() == city.lower()
            ]
        else:
            atm_store = self.atm_store

        return atm_store
