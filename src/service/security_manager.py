from src.helper.custom_exceptions import AuthenticationException


class SecurityManager:
    """Manage application security"""

    def __init__(self) -> None:
        self.default_username = "mobiquity"
        self.default_password = "mobiquity"

    def validate_authorization(self, context):
        """validate authorization context

        :context: dict
        """
        if (
            "username" in context
            and "password" in context
            and context["username"] == self.default_username
            and context["password"] == self.default_password
        ):
            pass
        else:
            raise AuthenticationException("Unauthorized access")
