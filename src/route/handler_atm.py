import logging

from flask import Blueprint, request

from src.service.atm_manager import AtmManager
from src.service.security_manager import SecurityManager
from src.helper.response import Response
from src.helper.custom_exceptions import AuthenticationException, ValidationException

handler_atm = Blueprint("handler_atm", __name__)

atm_manager = AtmManager()
security_manager = SecurityManager()



@handler_atm.before_request
def before_anything():
    """Filter to restrict unauthorized request"""
    try:
        security_manager.validate_authorization(request.headers)
    except AuthenticationException as ae:
        return Response.send_error("ING-501", str(ae), 401)


@handler_atm.route("/api/v1/atms", methods=["POST"])
def add_atm():
    """api to save a new atm record."""
    try:
        atm = request.get_json(silent=True)

        id = atm_manager.save(atm)

        response = Response.send(id, 201)
    except ValidationException as ve:
        response = Response.send_error("ING-502", str(ve), 400)
    except Exception as e:
        logging.exception(str(e))
        response = Response.send_error("ING-503", str(e))

    return response


@handler_atm.route("/api/v1/atms/<id>", methods=["PUT"])
def update_atm(id):
    """api to update an existing atm record."""
    try:
        atm = request.get_json(silent=True)

        result = atm_manager.update(id, atm)

        response = Response.send(result, 200)
    except ValidationException as ve:
        response = Response.send_error("ING-502", str(ve), 400)
    except Exception as e:
        logging.exception(str(e))
        response = Response.send_error("ING-503", str(e))

    return response


@handler_atm.route("/api/v1/atms/<id>", methods=["DELETE"])
def delete_atm_by_id(id):
    """api to delete an existing atm record."""
    try:
        if id:
            atm_manager.delete_by_id(id)

            response = Response.send(id)
        else:
            response = Response.send(id, 422)
    except Exception as e:
        logging.exception(str(e))
        response = Response.send_error("ING-503", str(e))

    return response


@handler_atm.route("/api/v1/atms")
def get_list_of_atms():
    """api to find existing atm records.

    :city: Optional query param.
    """
    try:
        response = Response.send(
            atm_manager.get_atms(
                request.args.get("city")))
    except Exception as e:
        logging.exception(str(e))
        response = Response.send_error("ING-503", str(e))

    return response
