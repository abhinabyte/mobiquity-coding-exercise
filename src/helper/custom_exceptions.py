import logging


class AuthenticationException(Exception):
    """Exception for authentication"""

    def __init__(self, msg=None):
        if msg is None:
            msg = "Authentication Exception"

        logging.exception(msg)

        super(AuthenticationException, self).__init__(msg)


class ValidationException(Exception):
    """Exception for data validation"""

    def __init__(self, msg=None):
        if msg is None:
            msg = "Invalid data format"

        logging.exception(msg)

        super(ValidationException, self).__init__(msg)
