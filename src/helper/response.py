from flask import jsonify


class Response:
    @staticmethod
    def send(data, status_code=200):
        return jsonify(success=True, data=data), status_code

    @staticmethod
    def send_error(code, desc, status_code=500):
        return (
            jsonify(success=False, errors=[{"code": code, "desc": desc}]),
            status_code,
        )
