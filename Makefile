install:
	pip3 install -r requirements.txt
	pip3 install -r requirements-dev.txt

black:
	black ./src ./tests 

flake:
	flake8 ./src ./tests  --count --max-complexity=10 --max-line-length=127 --statistics

autopep:
	autopep8 --in-place --aggressive --aggressive -a -r ./src ./tests
