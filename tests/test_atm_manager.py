from unittest import TestCase

from src.helper.custom_exceptions import ValidationException
from src.service.atm_manager import AtmManager


class TestAtmManager(TestCase):
    def setUp(self):
        self.atm_manager = AtmManager()
        self.data = {
            "address": {
                "street": "Prins Bernhardstraat",
                "housenumber": "1",
                "postalcode": "6566 CA",
                "city": "Millingen Aan De Rijn",
                "geoLocation": {"lat": "51.864646", "lng": "6.046826"},
            },
            "distance": 0,
            "openingHours": [
                {"dayOfWeek": 2, "hours": [{"hourFrom": "07:00", "hourTo": "23:00"}]},
                {"dayOfWeek": 3, "hours": [{"hourFrom": "07:00", "hourTo": "23:00"}]},
                {"dayOfWeek": 4, "hours": [{"hourFrom": "07:00", "hourTo": "23:00"}]},
                {"dayOfWeek": 5, "hours": [{"hourFrom": "07:00", "hourTo": "23:00"}]},
                {"dayOfWeek": 6, "hours": [{"hourFrom": "07:00", "hourTo": "23:00"}]},
                {"dayOfWeek": 7, "hours": [{"hourFrom": "07:00", "hourTo": "23:00"}]},
                {"dayOfWeek": 1, "hours": [{"hourFrom": "07:00", "hourTo": "23:00"}]},
            ],
            "functionality": "Geld storten en opnemen",
            "type": "GELDMAAT",
        }

    def test_load_atm_store(self):
        try:
            self.atm_manager.load_atm_store()
        except Exception:
            self.fail("Atm data feed loading failed")

    def test_save(self):
        id = self.atm_manager.save(self.data)

        self.assertIsNotNone(id)

    def test_save_with_error(self):
        try:
            self.atm_manager.save({})

            self.fail("Data validation failed")
        except ValidationException as e:
            self.assertTrue(
                "{'address': ['required field'], 'functionality': ['required field'], 'type': ['required field']}"
                in str(e)
            )

    def test_update(self):
        id = self.atm_manager.save(self.data)

        data = self.atm_manager.update(id, self.data)

        self.assertEqual(data["id"], id)

    def test_update_with_id_error(self):
        self.atm_manager.save(self.data)

        try:
            self.atm_manager.update("xyz", self.data)
        except ValidationException as e:
            self.assertTrue("No record exists with id xyz" in str(e))

    def test_update_with_validation_error(self):
        try:
            self.atm_manager.update("xyz", {})
        except ValidationException as e:
            self.assertTrue(
                "{'address': ['required field'], 'functionality': ['required field'], 'type': ['required field']}"
                in str(e)
            )

    def test_delete_by_id(self):
        try:
            self.atm_manager.delete_by_id("xyz")
        except Exception:
            self.fail("Delete functionlaity failed")

    def test_get_atms(self):
        atms = self.atm_manager.get_atms()

        self.assertEqual(0, len(atms))

        self.atm_manager.save(self.data)

        atms = self.atm_manager.get_atms()

        self.assertEqual(1, len(atms))

    def test_get_atms_by_city(self):
        self.atm_manager.save(self.data)

        atms = self.atm_manager.get_atms("Millingen Aan De Rijn")

        self.assertEqual(1, len(atms))

        atms = self.atm_manager.get_atms("abcd")

        self.assertEqual(0, len(atms))
