from unittest import TestCase

from src.helper.custom_exceptions import AuthenticationException
from src.service.security_manager import SecurityManager


class TestSecurityManager(TestCase):
    def setUp(self):
        self.security_manager = SecurityManager()

    def test_validate_authorization(self):
        try:
            self.security_manager.validate_authorization(
                {"username": "mobiquity", "password": "mobiquity"}
            )
        except AuthenticationException:
            self.fail("Security validation failed")

    def test_validate_authorization_error(self):
        try:
            self.security_manager.validate_authorization({})

            self.fail("Security validation failed")
        except AuthenticationException as e:
            self.assertTrue("Unauthorized access" in str(e))
