## What is this service?

This service provide REST apis to perform CRUD operations on ING ATMs data which is available [here](https://www.ing.nl/api/locator/atms/) and return a well formed JSON response

## How Does it Work?

It uses an in-memory data-structure i.e. list to store the atm list. At the appliction startup, it invoke the ING ATMs data feed and format the data to populate the list. Next, on each api call; this list is referred/manipulated.

## Setup

### Prerequisites
| Name          |    Version    |
| ------------- |:-------------:|
| Python        | 3.7.4         |

### Steps
* At the root directory execute following command to install the necessary dependencies:

```
make install
```

* Once done execute following command to start the application:
```
python app.py
```
* Within few seconds, the application should be up and running at port **8080**

## API

### Create ATM

```
POST /api/v1/atms
```

#### Sample Request Body
```JSON
{
    "address": {
        "city": "Jubbega",
        "geoLocation": {
            "lat": "53.004131",
            "lng": "6.123058"
        },
        "housenumber": "14",
        "postalcode": "8411 XD",
        "street": "Jelle van Damweg"
    },
    "distance": 0,
    "functionality": "Geld storten en opnemen",
    "id": "4544570d343046408fbf42428592bbb1",
    "openingHours": [
        {
            "dayOfWeek": 2,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 3,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 4,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 5,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 6,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 7,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 1,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        }
    ],
    "type": "GELDMAAT"
}
```

#### Sample Response Body
```JSON
{
    "data": "efd37029bf604d7e9f39eb1eeea72d97",
    "success": true
}
```



### Update ATM

```
PUT /api/v1/atms/<atm id>
```

#### Sample Request Body
```JSON
{
    "address": {
        "city": "Jubbega",
        "geoLocation": {
            "lat": "53.004131",
            "lng": "6.123058"
        },
        "housenumber": "14",
        "postalcode": "8411 XD",
        "street": "Jelle van Damweg"
    },
    "distance": 0,
    "functionality": "Geld storten en opnemen",
    "id": "4544570d343046408fbf42428592bbb1",
    "openingHours": [
        {
            "dayOfWeek": 2,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 3,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 4,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 5,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 6,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 7,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        },
        {
            "dayOfWeek": 1,
            "hours": [
                {
                    "hourFrom": "07:00",
                    "hourTo": "23:00"
                }
            ]
        }
    ],
    "type": "GELDMAAT"
}
```

#### Sample Response Body
```JSON
{
    "data": {
        "address": {
            "city": "Jubbega1112",
            "geoLocation": {
                "lat": "53.004131",
                "lng": "6.123058"
            },
            "housenumber": "14",
            "postalcode": "8411 XD",
            "street": "Jelle van Damweg"
        },
        "distance": 0,
        "functionality": "Geld storten en opnemen",
        "id": "efd37029bf604d7e9f39eb1eeea72d97",
        "openingHours": [
            {
                "dayOfWeek": 2,
                "hours": [
                    {
                        "hourFrom": "07:00",
                        "hourTo": "23:00"
                    }
                ]
            },
            {
                "dayOfWeek": 3,
                "hours": [
                    {
                        "hourFrom": "07:00",
                        "hourTo": "23:00"
                    }
                ]
            },
            {
                "dayOfWeek": 4,
                "hours": [
                    {
                        "hourFrom": "07:00",
                        "hourTo": "23:00"
                    }
                ]
            },
            {
                "dayOfWeek": 5,
                "hours": [
                    {
                        "hourFrom": "07:00",
                        "hourTo": "23:00"
                    }
                ]
            },
            {
                "dayOfWeek": 6,
                "hours": [
                    {
                        "hourFrom": "07:00",
                        "hourTo": "23:00"
                    }
                ]
            },
            {
                "dayOfWeek": 7,
                "hours": [
                    {
                        "hourFrom": "07:00",
                        "hourTo": "23:00"
                    }
                ]
            },
            {
                "dayOfWeek": 1,
                "hours": [
                    {
                        "hourFrom": "07:00",
                        "hourTo": "23:00"
                    }
                ]
            }
        ],
        "type": "GELDMAAT"
    },
    "success": true
}
```




















### Delete ATM

```
DELETE /api/v1/atms/<atm id>
```

#### Sample Response Body
```JSON
{
    "data": "efd37029bf604d7e9f39eb1eeea72d97",
    "success": true
}
```

### List All ATMs

```
GET /api/v1/atms
```

#### Sample Response Body
```JSON
{
    "data": [ ... ],
    "success": true
}
```

### List ATMs by city name

```
GET /api/v1/atms?city=<name>
```

#### Sample Response Body
```JSON
{
    "data": [ ... ],
    "success": true
}
```

In case of any error, api response Body will look like:
```JSON
{
    "errors": [
        {
            "code": "<error code>",
            "desc": "<error message>"
        }
    ],
    "success": false
}
```

## Consequences
Since, the application refer and polish the ING ATMs data feed at the application startup, it may take few seconds(depends on the bandwidth) to start the application.